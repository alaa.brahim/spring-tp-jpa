
package com.alaa.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alaa.product.entities.Product;
import com.alaa.product.repositories.ProductRepository;

@SpringBootApplication
public class ProductApplication implements CommandLineRunner {

	@Autowired
	private ProductRepository productRepository;

	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}

	/**
	 * Cette méthode est exécutée au démarrage de l'application.
	 * Elle est utilisée pour initialiser la base de données avec quelques produits,
	 * puis afficher les produits enregistrés et effectuer des requêtes de
	 * recherche.
	 *
	 * @param args Les arguments de la ligne de commande
	 * @throws Exception Si une exception se produit lors de l'exécution
	 */
	@Override
	public void run(String... args) throws Exception {

		productRepository.save(new Product(null, "Laptop", 1000, 10));
		productRepository.save(new Product(null, "Printer", 200, 5));
		productRepository.save(new Product(null, "Smartphone", 500, 20));
		System.out.println("----------------------");
		productRepository.findAll().forEach(p -> {
			System.out.println(p);
		});
		Product p = productRepository.findById(1L).get();
		System.out.println("----------------------");
		System.out.println("Product with id 1: " + p);
		System.out.println("----------------------");
		System.out.println("Products with name containing 'o': ");
		productRepository.findByNameContains("o").forEach(p1 -> {
			System.out.println(p1.getName());
		});
		System.out.println("----------------------");
		System.out.println("Products with name like '%e%': ");
		productRepository.search("%e%").forEach(p2 -> {
			System.out.println(p2.getName());
		});
		System.out.println("----------------------");
		System.out.println("Products with price greater than 300: ");
		productRepository.findByPriceGreaterThan(300).forEach(p3 -> {
			System.out.println(p3);
		});
		System.out.println("----------------------");
		System.out.println("Products with price greater than 300: ");
		productRepository.searchByPrice(300).forEach(p4 -> {
			System.out.println(p4);
		});

	}

}
