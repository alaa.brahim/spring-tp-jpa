package com.alaa.product.web;

import org.springframework.web.bind.annotation.RestController;

import com.alaa.product.entities.Product;
import com.alaa.product.repositories.ProductRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * Le contrôleur qui gère les requêtes HTTP relatives aux produits.
 */
@RestController
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    /**
     * Récupère tous les produits.
     *
     * @return la liste de tous les produits
     */
    @GetMapping("/products")
    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    /**
     * Récupère un produit par son identifiant.
     *
     * @param id l'identifiant du produit à récupérer
     * @return le produit correspondant à l'identifiant, ou null si aucun produit n'est trouvé
     */
    @GetMapping("/products/{id}")
    public Product getProduct(@PathVariable Long id) {
        return productRepository.findById(id).orElse(null);
    }

    /**
     * Récupère les produits dont le nom contient le mot-clé spécifié.
     *
     * @param keyword le mot-clé à rechercher dans le nom des produits
     * @return la liste des produits dont le nom contient le mot-clé
     */
    @GetMapping("/products/search/{keyword}")
    public List<Product> getProductsByName(@PathVariable String keyword) {
        return productRepository.findByNameContains(keyword);
    }

    /**
     * Récupère les produits dont le prix est supérieur au prix spécifié.
     *
     * @param price le prix minimum des produits à récupérer
     * @return la liste des produits dont le prix est supérieur au prix spécifié
     */
    @GetMapping("/products/search/price/{price}")
    public List<Product> getProductsByPrice(@PathVariable double price) {
        return productRepository.findByPriceGreaterThan(price);
    }

    /**
     * Enregistre un nouveau produit.
     *
     * @param product le produit à enregistrer
     * @return le produit enregistré
     */
    @PostMapping("/products")
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    /**
     * Supprime un produit par son identifiant.
     *
     * @param id l'identifiant du produit à supprimer
     */
    @DeleteMapping("/products/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productRepository.deleteById(id);
    }

    /**
     * Met à jour un produit par son identifiant.
     *
     * @param id l'identifiant du produit à mettre à jour
     * @param product le produit avec les nouvelles données
     * @return le produit mis à jour
     */
    @PutMapping("/products/{id}")
    public Product updateProduct(@PathVariable Long id, Product product) {
        product.setId(id);
        return productRepository.save(product);
    }
}
