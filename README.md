# Projet de gestion de produits

Ce projet vise à développer une application de gestion de produits en utilisant JPA et MySQL. L'application lance automatiquement une base de données MySQL et créer un conteneur grâce au support de Spring Compose.

## Endpoints des produits

L'application expose les endpoints suivants pour la gestion des produits :

- `GET /products` : Récupère tous les produits.
- `POST /products` : Crée un nouveau produit.
- `GET /products/{id}` : Récupère un produit spécifique en fonction de son ID.
- `PUT /products/{id}` : Met à jour un produit existant en fonction de son ID.
- `DELETE /products/{id}` : Supprime un produit existant en fonction de son ID.
- `GET /products/search/{keyword}` : Recherche des produits en fonction d'un mot-clé.
- `GET /products/search/price/{price}` : Recherche des produits en fonction d'un prix.

## Fonctionnement de l'application

L'application utilise les entités JPA, les repositories et les contrôleurs pour gérer les requêtes liées aux produits. Lorsqu'une requête est reçue, l'entité JPA correspondante est utilisée pour interagir avec la base de données via le repository. Le contrôleur gère ensuite la logique métier et renvoie la réponse appropriée.

## Swagger et OpenAPI

Je utilise Swagger pour documenter mon API. Vous pouvez consulter la documentation de l'API en utilisant le lien [Swagger OpenAPI](http://localhost:8080/swagger-ui.html).

![Swagger OpenAPI](swagger.png)
