package com.alaa.product.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.alaa.product.entities.Product;

/**
 * Cette interface représente le repository pour l'entité Product.
 * Elle étend l'interface JpaRepository pour bénéficier des opérations CRUD de base.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    
    /**
     * Recherche les produits dont le nom contient le mot-clé spécifié.
     * 
     * @param keyword le mot-clé à rechercher dans le nom des produits
     * @return une liste de produits correspondant à la recherche
     */
    List<Product> findByNameContains(String keyword);

    /**
     * Recherche les produits dont le nom est similaire au mot-clé spécifié.
     * 
     * @param keyword le mot-clé à rechercher dans le nom des produits
     * @return une liste de produits correspondant à la recherche
     */
    @Query("SELECT p FROM Product p WHERE p.name LIKE :keyword")
    List<Product> search(String keyword);

    /**
     * Recherche les produits dont le prix est supérieur au prix spécifié.
     * 
     * @param price le prix minimum des produits à rechercher
     * @return une liste de produits correspondant à la recherche
     */
    List<Product> findByPriceGreaterThan(double price);

    /**
     * Recherche les produits dont le prix est supérieur au prix spécifié.
     * 
     * @param price le prix minimum des produits à rechercher
     * @return une liste de produits correspondant à la recherche
     */
    @Query("SELECT p FROM Product p WHERE p.price > :price")
    List<Product> searchByPrice(double price);
}
